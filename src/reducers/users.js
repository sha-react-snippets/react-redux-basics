import * as actions from '../actions';

const initialState = {
  users: [],
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.STORE_USERS:
      return {
        users: action.payload,
      };

    case actions.REMOVE_USER:
      return {
        users: state.users.filter((u) => u.id !== action.payload),
      };

    default:
      return state;
  }
};

export default usersReducer;
