import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import * as actions from '../../actions';

import List from '../List';

class App extends Component {
  async componentDidMount() {
    const { storeUsers } = this.props;
    const response = await axios.get('https://jsonplaceholder.typicode.com/users');
    storeUsers(response.data);
  }

  render() {
    return (
      <List />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  storeUsers: (users) => dispatch({
    type: actions.STORE_USERS,
    payload: users,
  }),
});

App.propTypes = {
  storeUsers: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(App);
